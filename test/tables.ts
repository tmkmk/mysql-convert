export type ivt = [number | string, number, number?];
export const tables = {
  "about": { // 关于
    "id": [0, 0, 4294967295] as ivt, // 
    "createTime": [0, 0, 4294967295] as ivt, // 创建时间
    "changeTime": [0, 0, 4294967295] as ivt, // 修改时间
    "pid": [0, 0, 4294967295] as ivt, // 父级id
    "type": ["", 255] as ivt, // 类型
    "state": [0, 0, 4294967295] as ivt, // 状态
    "power": [0, 0, 4294967295] as ivt, // 权限
    "title": ["", 255] as ivt, // 标题
    "img": ["", 1024] as ivt, // 封面图片
    "simple": ["", 1024] as ivt, // 简易说明
    "content": ["", 65535] as ivt // 正
  },
  "culture": { // 企业文化
    "id": [0, 0, 4294967295] as ivt, // 
    "createTime": [0, 0, 4294967295] as ivt, // 创建时间
    "changeTime": [0, 0, 4294967295] as ivt, // 修改时间
    "pid": [0, 0, 4294967295] as ivt, // 父级id
    "type": ["", 255] as ivt, // 类型
    "state": [0, 0, 4294967295] as ivt, // 状态
    "power": [0, 0, 4294967295] as ivt, // 权限
    "title": ["", 255] as ivt, // 标题
    "img": ["", 1024] as ivt, // 封面图片
    "simple": ["", 1024] as ivt, // 简易说明
    "content": ["", 65535] as ivt // 正
  },
  "links": { // 友情链接
    "id": [0, 0, 4294967295] as ivt, // 
    "createTime": [0, 0, 4294967295] as ivt, // 创建时间
    "changeTime": [0, 0, 4294967295] as ivt, // 修改时间
    "pid": [0, 0, 4294967295] as ivt, // 父级id
    "type": ["", 255] as ivt, // 类型
    "state": [0, 0, 4294967295] as ivt, // 状态
    "power": [0, 0, 4294967295] as ivt, // 权限
    "title": ["", 255] as ivt, // 标题
    "img": ["", 1024] as ivt, // 封面图片
    "simple": ["", 1024] as ivt, // 简易说明
    "content": ["", 65535] as ivt // 正
  },
  "location": { // 办公地点
    "id": [0, 0, 4294967295] as ivt, // 
    "createTime": [0, 0, 4294967295] as ivt, // 创建时间
    "changeTime": [0, 0, 4294967295] as ivt, // 修改时间
    "pid": [0, 0, 4294967295] as ivt, // 父级id
    "type": ["", 255] as ivt, // 类型
    "state": [0, 0, 4294967295] as ivt, // 状态
    "power": [0, 0, 4294967295] as ivt, // 权限
    "title": ["", 255] as ivt, // 标题
    "img": ["", 1024] as ivt, // 封面图片
    "simple": ["", 1024] as ivt, // 简易说明
    "content": ["", 65535] as ivt // 正
  },
  "media": { // 媒体
    "id": [0, 0, 4294967295] as ivt, // 
    "createTime": [0, 0, 4294967295] as ivt, // 创建时间
    "changeTime": [0, 0, 4294967295] as ivt, // 修改时间
    "pid": [0, 0, 4294967295] as ivt, // 父级id
    "type": ["", 255] as ivt, // 类型
    "state": [0, 0, 4294967295] as ivt, // 状态
    "power": [0, 0, 4294967295] as ivt, // 权限
    "title": ["", 255] as ivt, // 标题
    "img": ["", 1024] as ivt, // 封面图片
    "simple": ["", 1024] as ivt, // 简易说明
    "content": ["", 65535] as ivt // 正
  },
  "service": { // 业务
    "id": [0, 0, 4294967295] as ivt, // 
    "createTime": [0, 0, 4294967295] as ivt, // 创建时间
    "changeTime": [0, 0, 4294967295] as ivt, // 修改时间
    "pid": [0, 0, 4294967295] as ivt, // 父级id
    "type": ["", 255] as ivt, // 类型
    "state": [0, 0, 4294967295] as ivt, // 状态
    "power": [0, 0, 4294967295] as ivt, // 权限
    "title": ["", 255] as ivt, // 标题
    "img": ["", 1024] as ivt, // 封面图片
    "simple": ["", 1024] as ivt, // 简易说明
    "content": ["", 65535] as ivt // 正
  },
  "setting": { // 设置
    "id": [0, 0, 4294967295] as ivt, // 
    "createTime": [0, 0, 4294967295] as ivt, // 创建时间
    "changeTime": [0, 0, 4294967295] as ivt, // 修改时间
    "pid": [0, 0, 4294967295] as ivt, // 父级id
    "type": ["", 255] as ivt, // 类型
    "state": [0, 0, 4294967295] as ivt, // 状态
    "power": [0, 0, 4294967295] as ivt, // 权限
    "title": ["", 255] as ivt, // 标题
    "img": ["", 1024] as ivt, // 封面图片
    "simple": ["", 1024] as ivt, // 简易说明
    "content": ["", 65535] as ivt // 正
  },
  "staff": { // 人员
    "id": [0, 0, 4294967295] as ivt, // 
    "createTime": [0, 0, 4294967295] as ivt, // 创建时间
    "changeTime": [0, 0, 4294967295] as ivt, // 修改时间
    "pid": [0, 0, 4294967295] as ivt, // 父级id
    "type": ["", 255] as ivt, // 类型
    "state": [0, 0, 4294967295] as ivt, // 状态
    "power": [0, 0, 4294967295] as ivt, // 权限
    "title": ["", 255] as ivt, // 标题
    "img": ["", 1024] as ivt, // 封面图片
    "simple": ["", 1024] as ivt, // 简易说明
    "content": ["", 65535] as ivt // 正
  },
  "users": { // 用户
    "id": [0, 0, 4294967295] as ivt, // 
    "createTime": [0, -2147483648, 2147483647] as ivt, // 创建时间
    "changeTime": [0, 0, 4294967295] as ivt, // 修改时间
    "type": ["", 255] as ivt, // 类型
    "state": [0, 0, 4294967295] as ivt, // 状态
    "power": [0, 0, 4294967295] as ivt, // 权限
    "username": ["", 16] as ivt, // 用户名
    "password": ["", 16] as ivt, // 密码
    "vi": ["", 65535] as ivt, // 私密信息
    "bi": ["", 65535] as ivt // 公开信
  }
};
export type itk = keyof typeof tables;
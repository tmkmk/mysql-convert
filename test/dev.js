const { Literal, TmkMysql } = require("../dist/index");
const { tables, itk } = require("../dist/tables");

const convert = new TmkMysql({
  table: "users",
  field: tables.users
});

const v = { "createTime": 1613804713, "machine": 1, "buy": 4, "sell": 3, "display": 255, "log": "[1613804713,4,{\"0\":[]}]", "service": 300000, "days": 5, "commission": 1050000, "increasing": 0, "price": 1350000 };
let q = {
  method: 2,
  table: "transaction",
  field: ["id"],
  where: [
    new Literal("(buy=4 or sell=4)"),
    { field: "id", value: 1 }
  ],
  value: v
};
// q = {
//   method: 0,
//   table: "users",
//   where: [{ field: "id", value: 0 }]
// };
// console.log(JSON.parse(JSON.stringify(convert.sql(q))));
console.log(convert.sql(q));
import { Where, curd } from "./struct";
export interface oa {
    [propName: string]: any;
}
export interface os {
    [propName: string]: string;
}
export declare type ns = number | string;
export declare type typeFieldMember = [ns, number, number?];
export interface typeField {
    [propName: string]: typeFieldMember;
}
export interface Create {
    field: typeField;
    table: string;
    limit?: number;
    offset?: number;
    auto?: string[];
    read?: string[];
    showField?: string[];
}
export declare type ck = keyof typeof curd;
export interface SqlObject {
    method: ck;
    table: string;
    where: Where[];
    value: oa;
    limit: number;
    offset: number;
    field: string[];
    other: string;
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.methodIn = exports.methodMerge = exports.clearEscapeChar = void 0;
function clearEscapeChar(p) {
    if (p) {
        if (typeof p != "string")
            p = JSON.stringify(p);
        return p.replace(/\\/g, "\\\\").replace(/'/g, "\\'").toString();
    }
    else if (typeof p == "number")
        return "0";
    return "";
}
exports.clearEscapeChar = clearEscapeChar;
;
function methodMerge(p, nc = false) {
    let ret = p.logic ? `${p.logic} ` : " ";
    ret += "`" + p.field + "`" + p.method;
    let t = nc ? p.value.toString() : clearEscapeChar(p.value);
    if (p.type.length == 2) {
        if (t.length > p.type[0]) {
            t = p.type[1] ? t.substr(0, p.type[1]) : t.substr(t.length - p.type[1]);
        }
        t = `'${t}'`;
    }
    ret += t;
    return ret;
}
exports.methodMerge = methodMerge;
function methodIn(p, not = false) {
    let r = "";
    if (Array.isArray(p.value)) {
        const d = [];
        p.value.forEach(a => {
            switch (typeof a) {
                case "number":
                    d.push(a);
                    break;
                case "string":
                    d.push(clearEscapeChar(a));
                    break;
                default:
                    break;
            }
        });
        p.value = d;
        r = `${p.logic || ""} \`${p.field}\` ${not ? "not " : ""}in (${p.value.join()})`;
    }
    return r;
}
exports.methodIn = methodIn;

"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Literal = exports.Where = exports.curd = exports.mtd = exports.MysqlObject = void 0;
const method = __importStar(require("@mac-xiang/method"));
const fun_1 = require("./fun");
class MysqlObject {
    constructor(param) {
    }
}
exports.MysqlObject = MysqlObject;
const int8 = Math.pow(2, 7);
const int16 = Math.pow(2, 15);
const int32 = Math.pow(2, 31);
const int64 = Math.pow(2, 63);
const intType = {
    int8: [0 - int8, int8 - 1],
    uint8: [0, (int8 * 2) - 1],
    int16: [0 - int16, int16 - 1],
    uint16: [0, (int16 * 2) - 1],
    int32: [0 - int32, int32 - 1],
    uint32: [0, (int32 * 2) - 1],
    int: [0 - int32, int32 - 1],
    uint: [0, (int32 * 2) - 1],
    int64: [0 - int64, int64 - 1],
    uint64: [0, (int64 * 2) - 1],
};
exports.mtd = {
    "=": (a) => { return fun_1.methodMerge(a); },
    ">": (a) => { return fun_1.methodMerge(a); },
    "<": (a) => { return fun_1.methodMerge(a); },
    ">=": (a) => { return fun_1.methodMerge(a); },
    "<=": (a) => { return fun_1.methodMerge(a); },
    "!=": (a) => { return fun_1.methodMerge(a); },
    "in": (a) => { return fun_1.methodIn(a); },
    "nin": (a) => { return fun_1.methodIn(a, true); },
    "regexp": fun_1.methodMerge,
    "like": (a) => { return ` %'${fun_1.methodMerge(a)}'%`; }
};
exports.curd = {
    0: "SELECT", 1: "UPDATE", 2: "INSERT INTO", 3: "DELETE FROM",
    get: 0, up: 1, add: 2, del: 3,
    r: 0, u: 1, c: 2, d: 3,
    read: 0, update: 1, create: 2, delete: 3
};
class Where {
    constructor(param, field) {
        this.field = "";
        this.logic = "and";
        this.method = "=";
        this.value = "";
        this.string = "";
        this.type = [0, 0];
        this.check = false;
        if (param instanceof Literal) {
            this.string = param.value;
            this.check = true;
        }
        if (param instanceof Where) {
            Object.assign(this, param);
        }
        else if (param && method.isNst(param.field) && (method.isNS(param.value) || Array.isArray(param.value))) {
            this.field = param.field;
            this.logic = param.logic != "or" ? "and" : "or";
            if (Array.isArray(param.value) && param.method != "nin")
                param.method = "in";
            else if (!param.method || typeof param.method != "string" || !exports.mtd[param.method])
                param.method = "=";
            this.method = param.method;
            this.value = param.value;
            this.type = field[this.field];
            this.string = exports.mtd[this.method](this);
            this.check = true;
        }
    }
}
exports.Where = Where;
class Literal {
    constructor(p) {
        this.value = "";
        this.field = "";
        if (typeof p == "string")
            this.value = p;
    }
}
exports.Literal = Literal;
exports.default = { MysqlObject, Where };

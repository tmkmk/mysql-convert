import { oa, ns, typeField, typeFieldMember } from "./itf";
import { methodMerge } from "./fun";
export declare class MysqlObject {
    [propName: string]: any;
    constructor(param: oa);
}
export declare const mtd: {
    "=": (a: Where) => string;
    ">": (a: Where) => string;
    "<": (a: Where) => string;
    ">=": (a: Where) => string;
    "<=": (a: Where) => string;
    "!=": (a: Where) => string;
    in: (a: Where) => string;
    nin: (a: Where) => string;
    regexp: typeof methodMerge;
    like: (a: Where) => string;
};
export declare type mtdK = keyof typeof mtd;
export declare const curd: {
    0: string;
    1: string;
    2: string;
    3: string;
    get: number;
    up: number;
    add: number;
    del: number;
    r: number;
    u: number;
    c: number;
    d: number;
    read: number;
    update: number;
    create: number;
    delete: number;
};
export declare class Where {
    field: string;
    logic: "and" | "or";
    method: mtdK;
    value: ns | ns[];
    string: string;
    type: typeFieldMember;
    readonly check: boolean;
    constructor(param: any, field: typeField);
}
export declare class Literal {
    value: string;
    field: string;
    constructor(p: string);
}
declare const _default: {
    MysqlObject: typeof MysqlObject;
    Where: typeof Where;
};
export default _default;

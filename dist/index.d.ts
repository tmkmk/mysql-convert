import { Create, oa, typeField } from "./itf";
import { Where, Literal } from "./struct";
export default class TmkMysql {
    field: typeField;
    table: string;
    limit: number;
    offset: number;
    auto: string[];
    read: string[];
    fk: string[];
    showField: string[];
    readonly check: boolean;
    constructor(param: Create);
    private Where2s;
    private value2s;
    private SqlObject2s;
    getMethod(p: any): 0 | 2 | 3 | 1 | "get" | "up" | "add" | "del" | "r" | "u" | "c" | "d" | "read" | "update" | "create" | "delete";
    getWhere(p: any): Where[];
    private formatValue;
    getValue(p: any, m: any): oa;
    getField(p: any): string[];
    sql(p: oa): string;
}
export { Literal, TmkMysql };

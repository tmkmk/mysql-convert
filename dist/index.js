"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TmkMysql = exports.Literal = void 0;
const method_1 = __importDefault(require("@mac-xiang/method"));
const struct_1 = require("./struct");
Object.defineProperty(exports, "Literal", { enumerable: true, get: function () { return struct_1.Literal; } });
const fun_1 = require("./fun");
class TmkMysql {
    constructor(param) {
        this.field = {};
        this.table = "";
        this.limit = 100;
        this.offset = 0;
        this.auto = ["id"];
        this.read = ["id"];
        this.fk = [];
        this.showField = [];
        this.check = false;
        if (typeof param.field == "object" && method_1.default.isStringT(param.table)) {
            this.field = param.field;
            this.fk = Object.keys(this.field);
            this.table = param.table;
            if (method_1.default.isInt(param.limit))
                this.limit = parseInt(param.limit.toString());
            if (method_1.default.isInt(param.offset))
                this.offset = parseInt(param.offset.toString());
            if (Array.isArray(param.auto)) {
                const d = [];
                param.auto.map(a => {
                    if (method_1.default.isStringT(a) && this.fk.indexOf(a) >= 0) {
                        d.push(a);
                    }
                });
                this.auto = d;
            }
            if (Array.isArray(param.read)) {
                const d = [];
                param.read.forEach(a => {
                    if (method_1.default.isStringT(a) && this.fk.indexOf(a) >= 0) {
                        d.push(a);
                    }
                });
                this.read = d;
            }
            if (Array.isArray(param.showField)) {
                param.showField.forEach(a => {
                    if (typeof a == "string" && this.field[a]) {
                        this.showField.push(a);
                    }
                });
            }
            else
                this.showField = Object.keys(this.field);
        }
    }
    Where2s(p) {
        const ra = p.map(w => {
            return w.string;
            // return `${w.logic} \`${w.field}\` ${w.method} ${w.value}`;
        });
        let ret = ra.join(" ");
        while (ret.substr(0, 1) == " ") {
            ret = ret.substr(1);
        }
        let s = 0;
        if (ret.substr(0, 3) == "or ") {
            s = 2;
        }
        else if (ret.substr(0, 4) == "and ") {
            s = 3;
        }
        return ret.substr(s);
    }
    value2s(value, insert = false) {
        let ret = "";
        if (insert) {
            const f = this.fk.filter(a => {
                return this.auto.indexOf(a) < 0;
            });
            const v = f.map(a => {
                return this.formatValue(value[a], this.field[a]);
            });
            ret = `(\`${f.join("`,`")}\`) VALUES (${v.join(",")})`;
        }
        else {
            const ra = Object.keys(value).map(w => {
                return `\`${w}\`=${this.formatValue(value[w], this.field[w])}`;
            });
            ret = ra.join(",");
        }
        return ret;
    }
    SqlObject2s(fmt) {
        let ret = struct_1.curd[fmt.method];
        let w = this.Where2s(fmt.where);
        if (w)
            w = "WHERE" + w;
        const v = this.value2s(fmt.value, ret == struct_1.curd[2]);
        switch (ret) {
            case struct_1.curd[0]: // 读
                ret = `${ret} \`${fmt.field.join("`,`")}\` FROM \`${fmt.table}\` ${w} ${fmt.other} LIMIT ${fmt.offset},${fmt.limit}`;
                break;
            case struct_1.curd[1]: // 改
                ret = v ? `${ret} \`${fmt.table}\` SET ${v} ${w}` : "";
                break;
            case struct_1.curd[2]: // 增
                ret = v ? `${ret} \`${fmt.table}\` ${v}` : "";
                break;
            case struct_1.curd[3]: // 删
                ret = w ? `${ret} \`${fmt.table}\` ${w}` : "";
                break;
            default:
                ret = "";
                break;
        }
        return ret;
    }
    getMethod(p) {
        let ret = struct_1.curd[p];
        if (!ret)
            ret = 0;
        else if (typeof ret == "string")
            ret = p;
        return ret;
    }
    getWhere(p) {
        let r = [];
        if (Array.isArray(p)) {
            p.forEach(a => {
                const e = new struct_1.Where(a, this.field);
                if (e.check && (this.fk.indexOf(e.field) >= 0 || !e.field))
                    r.push(e);
            });
        }
        return r;
    }
    formatValue(p, f) {
        let r = "";
        if (typeof p == "undefined") {
            r = f.length == 3 ? f[0] : `'${fun_1.clearEscapeChar(p)}'`;
        }
        else {
            if (p instanceof struct_1.Literal) {
                r = p.value;
            }
            else if (f.length == 2) {
                let s = fun_1.clearEscapeChar(p);
                s = s.substr(0 - f[1]);
                r = `'${s}'`;
            }
            else {
                const n = Number(p) ? Number(p) : f[0];
                const f2 = f[2];
                if (f[1] <= n && n <= f2)
                    r = n;
                else
                    r = f[0];
            }
        }
        return r;
    }
    getValue(p, m) {
        const r = {};
        if (method_1.default.type(p) == "object") {
            const read = this.fk.filter(k => {
                const f = this.field[k];
                if ((m == 2 || (m == 1 && typeof p[k] != "undefined")) &&
                    this.auto.indexOf(k) < 0) {
                    // p[k] = this.formatValue(p[k], f);
                    return true;
                }
            });
            Object.keys(p).forEach(k => {
                if (read.indexOf(k) >= 0)
                    r[k] = p[k];
                // if (read.indexOf(k) >= 0) {
                //   if (p[k] instanceof Literal) {
                //     r[k] = p[k].value;
                //   } else {
                //     if (this.field[k].length > 2) {
                //       r[k] = Number(p[k]);
                //       if (!(r[k] >= 0 || r[k] < 0)) r[k] = this.field[k][0];
                //     } else {
                //       r[k] = `'${clearEscapeChar(p[k])}'`;
                //     }
                //   }
                // }
            });
        }
        return r;
    }
    getField(p) {
        let r = this.showField;
        if (Array.isArray(p)) {
            r = [];
            p.forEach(a => {
                if (this.field[a])
                    r.push(a);
            });
        }
        return r;
    }
    sql(p) {
        p.method = this.getMethod(p.method);
        const fmt = {
            method: p.method,
            table: (p.table instanceof struct_1.Literal) ? p.table.value : this.table,
            where: this.getWhere(p.where),
            value: this.getValue(p.value, p.method),
            limit: method_1.default.isUint(p.limit) ? p.limit : this.limit,
            offset: method_1.default.isUint(p.offset) ? p.offset : this.offset,
            field: this.getField(p.field),
            other: p.other instanceof struct_1.Literal ? p.other.value : ""
        };
        return this.SqlObject2s(fmt);
    }
}
exports.default = TmkMysql;
exports.TmkMysql = TmkMysql;

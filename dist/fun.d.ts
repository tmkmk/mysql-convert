import * as struct from "./struct";
export declare function clearEscapeChar(p: any): string;
export declare function methodMerge(p: struct.Where, nc?: boolean): string;
export declare function methodIn(p: struct.Where, not?: any): string;
